package com.example.browserapp

import android.content.res.Configuration
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.example.browserapp.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.toolbar)
        updateActionBarTitle()
        initViews()
    }

    private fun initViews() {
        binding.apply {
            btnSearchButton.setBackgroundColor(
                ContextCompat.getColor(
                    this@MainActivity,
                    R.color.blue
                )
            )

            webView.settings.javaScriptEnabled = true

            webView.webViewClient = object : WebViewClient() {
                override fun onPageFinished(view: WebView?, url: String?) {
                    super.onPageFinished(view, url)
                    updateActionBarTitle()
                }
            }

            btnSearchButton.setOnClickListener {
                val link = etSearchEditText.text.toString()
                webView.apply {
                    clearCache(true)
                    loadUrl(link)
                }
            }

        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        if (resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            menuInflater.inflate(R.menu.action_bar_menu_landscape, menu)
        } else {
            menuInflater.inflate(R.menu.action_bar_menu_portrait, menu)
        }
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.menu_item1 -> {
                if (resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) {
                    binding.btnSearchButton.setBackgroundColor(
                        ContextCompat.getColor(
                            this,
                            R.color.brown
                        )
                    )
                } else {
                    binding.btnSearchButton.setBackgroundColor(
                        ContextCompat.getColor(
                            this,
                            R.color.green
                        )
                    )
                }
                true
            }
            R.id.menu_item2 -> {
                if (resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) {
                    binding.btnSearchButton.setBackgroundColor(
                        ContextCompat.getColor(
                            this,
                            R.color.yellow
                        )
                    )
                } else {
                    binding.btnSearchButton.setBackgroundColor(
                        ContextCompat.getColor(
                            this,
                            R.color.blue
                        )
                    )
                }
                true
            }
            R.id.menu_item3 -> {
                if (resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) {
                    binding.btnSearchButton.setBackgroundColor(
                        ContextCompat.getColor(
                            this,
                            R.color.orange
                        )
                    )
                } else {
                    binding.btnSearchButton.setBackgroundColor(
                        ContextCompat.getColor(
                            this,
                            R.color.violet
                        )
                    )
                }
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun updateActionBarTitle() {
        val title = if (binding.webView.title.isNullOrEmpty()) {
            getString(R.string.app_name)
        } else {
            binding.webView.title
        }
        supportActionBar?.title = title
    }
}